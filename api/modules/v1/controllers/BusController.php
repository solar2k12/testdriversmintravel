<?php

namespace api\modules\v1\controllers;

/**
 * Контроллер для получения информации о доступных автобусах
 * Class BusController
 * @package api\modules\v1\controllers
 */
class BusController extends UnauthorizedActiveController
{
    public $modelClass = 'api\modules\v1\models\Bus';

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
}