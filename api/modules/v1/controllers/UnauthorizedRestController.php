<?php

namespace api\modules\v1\controllers;

use yii\rest\Controller;

/**
 * Контроллер для работы с REST, который не требует идентифицировать пользователя(cookie)
 * Class UnauthorizedRestController
 * @package api\modules\v1\controllers
 */
abstract class UnauthorizedRestController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['rateLimiter']);
        return $behaviors;
    }
}