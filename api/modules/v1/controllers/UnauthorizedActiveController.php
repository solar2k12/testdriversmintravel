<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;

/**
 * Контроллер для работы с моделями, который не требует идентифицировать пользователя(cookie)
 * Class UnauthorizedActiveController
 * @package api\modules\v1\controllers
 */
abstract class UnauthorizedActiveController extends ActiveController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['rateLimiter']);
        return $behaviors;
    }
}