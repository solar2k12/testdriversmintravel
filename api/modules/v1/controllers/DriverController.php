<?php


namespace api\modules\v1\controllers;

use api\modules\v1\models\Driver;
use yii\data\ActiveDataProvider;

/**
 * Контроллер для получения информации о водителях
 * Class DriverController
 * @package api\modules\v1\controllers
 */
class DriverController extends UnauthorizedActiveController
{
    public $modelClass = 'api\modules\v1\models\Driver';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = function () {
            return new ActiveDataProvider([
                'query' => Driver::find(),
                'sort' => [
                    'defaultOrder' => [
                        'full_name' => SORT_ASC
                    ]
                ]
            ]);
        };

        return $actions;
    }
}