<?php


namespace api\modules\v1\controllers;

use api\services\calculatetime\TravelTimeService;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\di\NotInstantiableException;
use yii\web\NotFoundHttpException;

/**
 * Контроллер для расчета времени поездки между двумя населенными пунктами
 * Class TravelController
 * @package api\modules\v1\controllers
 */
class TravelController extends UnauthorizedRestController
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * Метод для расчета времени прохождения поездки
     * @param $from название населенного пункта отправки
     * @param $to название населенного пункта назначения
     * @param int | null $id идентификатор водителя для которого нужен расчет времени(опционально)
     * @return ActiveDataProvider
     * @throws Exception
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     * @throws NotInstantiableException
     */
    public function actionIndex($from, $to, $id = null)
    {
        /** @var TravelTimeService $travelTimeService */
        $travelTimeService = Yii::$container->get('travelTimeService');
        $hoursPerDay = Yii::$app->params['hoursPerDay'];
        $query = $travelTimeService->getDriversTravelTime($from, $to, $hoursPerDay, $id);
        if (isset($id)) {
            $driver = $query->createCommand()->queryAll();
            if (empty($driver)) {
                throw new NotFoundHttpException(sprintf('Водитель с идентификатором %d не найден', $id));
            }
            return $driver;
        }

        $pageSize = Yii::$app->params['pageSize'];

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ]
        ]);
    }
}