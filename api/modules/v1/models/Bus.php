<?php

namespace api\modules\v1\models;

use yii\db\ActiveRecord;

/**
 * Модель для описания автобуса
 * Class Bus
 * @package api\modules\v1\models
 */
class Bus extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{bus}}';
    }
}