<?php


namespace api\modules\v1\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Модель для описания водителя
 * Class Drivers
 * @package api\modules\v1\models
 */
class Driver extends ActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{driver}}';
    }

    /**
     * @return array|false
     */
    public function fields()
    {
        $fields = [
            'id',
            'name' => function(Driver $driver) {
                return $driver->full_name;
            },
            'age',
            'birth_date'
        ];
        if (Yii::$app->controller->action->id === 'view') {
            $fields[] = 'vehicles';
        }

        return $fields;

    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getAge()
    {
        return date_diff(new \DateTime(), date_create_from_format('Y-m-d', $this->birth_date))->y;
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getVehicles()
    {
        return $this->hasMany(Bus::class, ['id' => 'bus_id'])
            ->viaTable('driver_bus', ['driver_id'=>'id']);
    }
}