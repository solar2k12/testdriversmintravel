<?php

namespace api\modules\v1;

use yii\base\Module;

/**
 * Class TravelAPIModule
 * @package api\modules\v1
 */
class TravelAPIModule extends Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();
    }
}