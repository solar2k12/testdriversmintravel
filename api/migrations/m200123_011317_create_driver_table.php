<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%driver}}`.
 */
class m200123_011317_create_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableName = $this->db->tablePrefix . 'driver';
        if ($this->db->getTableSchema($tableName, true) === null) {
            $this->createTable('{{%driver}}', [
                'id' => $this->primaryKey(),
                'full_name' => $this->string()->notNull(),
                'birth_date' => $this->date()->notNull()
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $tableName = $this->db->tablePrefix . 'driver';
        if ($this->db->getTableSchema($tableName, true) !== null) {
            $this->dropTable('{{%driver}}');
        }
    }
}
