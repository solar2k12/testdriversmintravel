<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bus}}`.
 */
class m200123_012211_create_bus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = $this->db->tablePrefix . 'bus';
        if ($this->db->getTableSchema($tableName, true) === null) {
            $this->createTable('{{%bus}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string()->notNull(),
                'speed' => $this->tinyInteger()->notNull()->unsigned()
            ]);
            $this->createIndex('idx-bus-speed', $tableName, 'speed');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $tableName = $this->db->tablePrefix . 'bus';
        if ($this->db->getTableSchema($tableName, true) !== null) {
            $this->dropTable('{{%bus}}');
        }
    }
}
