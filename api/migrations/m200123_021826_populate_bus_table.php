<?php

use Faker\Provider\Fakecar;
use yii\db\Migration;

/**
 * Class m200123_021826_populate_bus_table
 */
class m200123_021826_populate_bus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        srand();
        $busCount = rand(Yii::$app->params['minVehicles'], Yii::$app->params['maxVehicles']);
        $buses = [];
        $faker = Faker\Factory::create();
        $faker->addProvider(new Fakecar($faker));
        foreach (range(1, $busCount) as $index) {
            srand();
            $year = $faker->dateTimeBetween('-20 years')->format('Y');
            $name = sprintf('%s %s %s', $faker->vehicleBrand, $faker->vehicleModel, $year);
            $buses[] = [
                $name,
                rand(Yii::$app->params['minSpeed'], Yii::$app->params['maxSpeed'])

            ];
        }
        $this->batchInsert(
            '{{%bus}}',
            [
                'name',
                'speed'
            ],
            $buses
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m200123_021826_populate_bus_table cannot be reverted.\n";

        return false;
    }
}
