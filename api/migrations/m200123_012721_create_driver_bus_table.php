<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%driver_bus}}`.
 */
class m200123_012721_create_driver_bus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = $this->db->tablePrefix . 'driver_bus';
        if ($this->db->getTableSchema($tableName, true) === null) {
            $this->createTable('{{%driver_bus}}', [
                'driver_id' => $this->integer()->notNull(),
                'bus_id' => $this->integer()->notNull(),
                'PRIMARY KEY(driver_id, bus_id)'
            ]);
            $this->createIndex('idx-driver_bus-driver_id', $tableName, 'driver_id');
            $this->addForeignKey(
                'fk-driver_bus-driver_id',
                $tableName,
                'driver_id',
                'driver',
                'id',
                'CASCADE'
            );
            $this->createIndex('idx-driver_bus-bus_id', $tableName, 'bus_id');
            $this->addForeignKey(
                'fk-driver_bus-bus_id',
                $tableName,
                'bus_id',
                'bus',
                'id',
                'CASCADE'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = $this->db->tablePrefix . 'driver_bus';
        if ($this->db->getTableSchema($tableName, true) !== null) {
            $this->dropForeignKey('fk-driver_bus-driver_id', $tableName);
            $this->dropForeignKey('fk-driver_bus-bus_id', $tableName);
            $this->dropTable('{{%driver_bus}}');
        }
    }
}
