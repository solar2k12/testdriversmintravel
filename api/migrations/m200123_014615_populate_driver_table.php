<?php

use yii\db\Migration;

/**
 * Class m200123_014615_populate_driver_table
 */
class m200123_014615_populate_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        srand();
        $driversCount = rand(Yii::$app->params['minDrivers'], Yii::$app->params['maxDrivers']);
        $drivers = [];
        $faker = Faker\Factory::create();
        foreach (range(1, $driversCount) as $index) {
            $drivers[] = [
                $faker->name(),
                $faker->dateTimeBetween('-65 years', '-18 years')->format('Y-m-d')
            ];
        }
        $this->batchInsert(
            '{{%driver}}',
            [
                'full_name',
                'birth_date'
            ],
            $drivers
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        echo "m200123_014615_populate_driver_table cannot be reverted.\n";

        return false;
    }
}
