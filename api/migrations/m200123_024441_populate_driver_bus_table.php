<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m200123_024441_populate_driver_bus_table
 */
class m200123_024441_populate_driver_bus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $drivers = (new Query())->select('id')
            ->from('driver')
            ->all();
        foreach ($drivers as $driver) {
            $this->execute(
                sprintf(
                    'INSERT INTO driver_bus(driver_id, bus_id)
                        SELECT %d as driver_id, r1.id as bus_id
                        FROM bus AS r1 JOIN
                                    (SELECT CEIL(RAND() *
                                        (SELECT MAX(id)
                                           FROM bus)) AS id)
                                 AS r2
                        WHERE r1.id >= r2.id
                        ORDER BY r1.id ASC',
                    $driver['id']
                )
            );
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200123_024441_populate_driver_bus_table cannot be reverted.\n";

        return false;
    }
}
