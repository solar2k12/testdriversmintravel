<?php

namespace api\services\calculatetime\repository;

use yii\db\Query;

/**
 * Интерфейс репозитория для определния самого оптимального транспортного средства,
 * доступного водиителю при поездке на определенное расстояние
 * Interface IDriverTravelTimeRepository
 * @package api\services\calculatetime
 */
interface IDriverTravelTimeRepository
{
    /**
     * Возвращает список параметров водителя или водителей
     * и минимальное количество дней за которое он может достичь точки назначения на расстоянии $distance
     * @param int $distance - расстояние до точки назначения
     * @param int $hoursPerDay - максимальное количество часов в день за рулем
     * @param int | null $id - идентификатор водителя для расчета
     * @return Query
     */
    public function getDriverTravelDaysQuery($distance, $hoursPerDay = 8, $id = null);
}