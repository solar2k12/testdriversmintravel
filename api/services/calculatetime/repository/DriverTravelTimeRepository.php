<?php

namespace api\services\calculatetime\repository;

use yii\db\Query;

/**
 * Репозиторий для работы с водителями и доступным автопарком для расчета времени прибытия
 * Class DriverTravelTimeRepository
 * @package api\services\calculatetime
 */
class DriverTravelTimeRepository implements IDriverTravelTimeRepository
{
    /**
     * @inheritDoc
     */
    public function getDriverTravelDaysQuery($distance, $hoursPerDay = 8, $id = null)
    {
        $dbSpeedSubQueryJoinCondition = 'driver_bus.bus_id = bus.id';
        if (isset($id)) {
            $dbSpeedSubQueryJoinCondition.= ' AND driver_bus.driver_id = :id';
        }

        $dbSpeedSubQuery = (new Query())->select(['driver_bus.driver_id as driver_id', 'bus.speed as speed'])
            ->from('bus')
            ->innerJoin('driver_bus', $dbSpeedSubQueryJoinCondition);

        $maxSpeedSubQuery = (new Query())->select(['driver_id', 'MAX(speed) AS maxSpeed'])->from($dbSpeedSubQuery)
            ->groupBy('driver_id');

        $query = (new Query())->select([
            'd.id as id',
            'd.full_name as name',
            'd.birth_date as birth_date',
            'YEAR(CURRENT_DATE) - YEAR(d.birth_date) as age',
            'CEIL(:distance / (spd.maxSpeed * :hours_per_day)) as travel_time'
        ])->from(['spd'=>$maxSpeedSubQuery])
            ->innerJoin(['d' => 'driver'], 'spd.driver_id = d.id')
            ->orderBy(['travel_time' => SORT_ASC]);
        $query->addParams([':distance' => $distance, ':hours_per_day' => $hoursPerDay]);
        if (isset($id)) {
            $query->addParams([':id' => $id]);
        }

        return $query;
    }
}