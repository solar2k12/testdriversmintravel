<?php

namespace api\services\calculatetime;

use api\services\calculatetime\repository\IDriverTravelTimeRepository;
use api\services\distance\IDistanceService;
use yii\db\Query;

/**
 * Сервис для расчета времени прохождения
 * Class TravelTimeService
 * @package api\services\calculatetime
 */
class TravelTimeService
{
    /**
     *
     * @var IDistanceService
     */
    private $distanceService;

    /**
     * @var IDriverTravelTimeRepository
     */
    private $driverTravelRepo;

    /**
     * TravelTimeService constructor.
     * @param IDistanceService $distanceService
     * @param IDriverTravelTimeRepository $driverTravelRepo
     */
    public function __construct(IDistanceService $distanceService, IDriverTravelTimeRepository $driverTravelRepo)
    {
        $this->distanceService = $distanceService;
        $this->driverTravelRepo = $driverTravelRepo;
    }

    /**
     * Получить список водитель с расчетным количество дней в пути из города $from в $to
     * @param string $from
     * @param string $to
     * @param integer $hoursPerDay
     * @param null | int $id
     * @return Query
     */
    public function getDriversTravelTime($from, $to, $hoursPerDay, $id = null)
    {
        $distance = $this->distanceService->getDistance($from, $to);
        return $this->driverTravelRepo->getDriverTravelDaysQuery($distance, $hoursPerDay, $id);
    }
}