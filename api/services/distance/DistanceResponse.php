<?php


namespace api\services\distance;

use api\services\distance\exceptions\InvalidPointException;
use yii\helpers\ArrayHelper;

/**
 * Объект содержит ответ от сервиса расчета расстояния DistanceAPI
 * Class DistanceResponse
 * @package api\services\distance
 */
class DistanceResponse
{
    // Название поля, которое содержит расстояние между указанными объектами
    const DISTANCE_FIELD_NAME = 'distance';

    // Название поля, которое содержит описание точек назначения
    const STOPS_FIELD_NAME = 'stops';

    // Название типа указанных точек для города
    const CITY_TYPE = 'City';

    /**
     * Расстояние между точками
     * @var integer
     */
    private $distance;

    /**
     * Точка отправки
     * @var string
     */
    private $from;

    /**
     * Точка назначения
     * @var string
     */
    private $to;

    /**
     * Создает объект DistanceResponse и валидирует данные $data
     * DistanceResponse constructor.
     * @param array $data - данные ответа из сервиса Distance API
     * @throws \HttpResponseException
     */
    public function __construct(array $data)
    {
        $this->distance = ArrayHelper::getValue($data, self::DISTANCE_FIELD_NAME, null);
        if (!is_numeric($this->distance) || $this->distance < 0) {
            throw new \HttpResponseException(
                'Формат ответа от сервиса расчета расстояния не соответствует ожидаемому'
            );
        }
        settype($this->distance, 'integer');
        $stops = ArrayHelper::getValue($data, self::STOPS_FIELD_NAME, []);

        if (count($stops) !== 2) {
            throw new \HttpResponseException(
                'Формат ответа от сервиса расчета расстояния не соответствует ожидаемому'
            );
        }

        list($from, $to) = $stops;

        if (ArrayHelper::getValue($from, 'type', false) !== self::CITY_TYPE) {
            throw new InvalidPointException(
                'Точка отправки не является городом'
            );
        }

        if (ArrayHelper::getValue($from, 'type', false) !== self::CITY_TYPE) {
            throw new InvalidPointException(
                'Точка назначения не является городом'
            );
        }
    }

    /**
     * Получить расстояние между двумя точками
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }
}