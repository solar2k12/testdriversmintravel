<?php


namespace api\services\distance;

/**
 * Общий интерфейс для сервисов получения расстояния между городами
 * Interface IDistanceService
 * @package api\services\distance
 */
interface IDistanceService
{
    /**
     * Получить расстояние между городами $from и $to
     * @param string $from
     * @param string $to
     * @return integer
     */
    public function getDistance($from, $to);
}