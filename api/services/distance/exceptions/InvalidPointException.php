<?php


namespace api\services\distance\exceptions;

/**
 * Исключение когда одна из точек не является городом
 * Class InvalidPointException
 * @package api\services\distance\exceptions
 */
class InvalidPointException extends \LogicException {}