<?php


namespace api\services\distance\exceptions;

/**
 * Исключение при некорректном ответе сервиса
 * Class DistanceServiceResponseException
 * @package api\services\distance\exceptions
 */
class DistanceServiceResponseException extends \HttpResponseException {}