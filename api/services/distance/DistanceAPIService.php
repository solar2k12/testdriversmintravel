<?php

namespace api\services\distance;

use api\services\distance\exceptions\InvalidPointException;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\httpclient\Response;

/**
 * Сервис расчета расстояния Distance API
 * url: https://www.rasstoyanie.com
 * Class DefaultDistanceService
 * @package api\components
 */
final class DistanceAPIService implements IDistanceService
{
    /**
     * Начальный URL для работы c API сервиса
     * @var string
     */
    private $serviceEntryUrl;

    /**
     * Http клиент для отправки запроса
     * @var Client
     */
    private $httpClient;

    /**
     * DistanceAPIService constructor.
     * @param string $url
     * @param Client $httpClient
     * @throws \InvalidArgumentException
     */
    public function __construct($url, Client $httpClient)
    {
        if (!($this->serviceEntryUrl = filter_var($url, FILTER_VALIDATE_URL))) {
            throw new \InvalidArgumentException('В качестве параметра ожидается URL');
        }
        $this->httpClient = $httpClient;
    }

    /**
     * @inheritDoc
     */
    public function getDistance($from, $to, $units = 'km')
    {
        if (empty($from) || empty($to)) {
            throw new InvalidPointException('Требуется указать точку отправки и назначения');
        }

        if ($from === $to) {
            throw new InvalidPointException('Точка отправки и назначения совпадают');
        }

        $response = $this->executeRequest($from, $to);
        if (!$response->getIsOk()) {
            throw new \RuntimeException('Сервис расчета расстояния ответил ошибкой');
        }

        return (new DistanceResponse($response->getData()))->getDistance();
    }

    /**
     * Выполнить запрос
     * @param string $from
     * @param string $to
     * @return Response
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    private function executeRequest($from, $to)
    {
        return $this->httpClient->createRequest()
            ->setMethod('GET')
            ->setUrl(sprintf('%s?stops=%s|%s', $this->serviceEntryUrl, $from, $to))
            ->send();

    }
}