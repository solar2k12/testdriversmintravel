<?php
$db = require __DIR__ . '/db.php';
$params = require __DIR__ . '/params.php';
return [
    'id' => 'driver-travel-console',
    'basePath' => __DIR__ . '/../',
    'components' => [
        'db' => $db
    ],
    'params' => $params
];