<?php

use api\services\calculatetime\repository\DriverTravelTimeRepository;
use api\services\calculatetime\repository\IDriverTravelTimeRepository;
use api\services\distance\DistanceAPIService;
use api\services\distance\IDistanceService;
use yii\di\Instance;
use yii\web\Response;

$db = require __DIR__ . '/db.php';
$params = require __DIR__ . '/params.php';
return [
    'id' => 'driver-travel-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
        ]
    ],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\TravelAPIModule'
        ]
    ],
    'aliases' => [
        '@api' => dirname(dirname(__DIR__)) . '/api',
    ],
    'components' => [
        'db' => $db,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern' => '/v1/driver:<id:\d+>',
                    'route' => '/v1/driver/view',
                ],
                [
                    'pattern' => '/v1/driver/<page:\d+>',
                    'route' => '/v1/driver/index',
                    'defaults' => ['page' => 1],
                ],
                [
                    'pattern' => '/v1/bus:<id:\d+>',
                    'route' => '/v1/bus/view',
                ],
                [
                    'pattern' => '/v1/bus/<page:\d+>',
                    'route' => '/v1/bus/index',
                    'defaults' => ['page' => 1],
                ],
                [
                    'pattern' => '/v1/travel/<from:\w+>:<to:\w+>/<page:\d+>',
                    'route' => '/v1/travel/index',
                    'defaults' => ['page' => 1],
                ],
                [
                    'pattern' => '/v1/travel/<from:\w+>:<to:\w+>:<id:\d+>',
                    'route' => '/v1/travel/index',
                    'defaults' => ['id' => null],
                ],
            ]
        ]
    ],
    'container' => [
        'singletons' => [
            //Позже можно добавить сервис google distance matrix api(есть триал), используя IDistanceService
            IDistanceService::class => function () {
                return new DistanceAPIService(
                    'https://www.rasstoyanie.com/route.json',
                    new yii\httpclient\Client()
                );
            },
            IDriverTravelTimeRepository::class => function () {
                return new DriverTravelTimeRepository();
            },
            'travelTimeService' => [
                ['class' => '\api\services\calculatetime\TravelTimeService'],
                [
                    Instance::of(IDistanceService::class),
                    Instance::of(IDriverTravelTimeRepository::class)
                ]
            ]
        ]
    ],

    'params' => $params
];