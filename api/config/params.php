<?php
return [
    'minDrivers' => 10,
    'maxDrivers' => 50,
    'minVehicles' => 5,
    'maxVehicles' => 15,
    'minSpeed' => 30,
    'maxSpeed' => 100,
    'minBusCount' => 1,
    'maxBusCount' => 5,
    'hoursPerDay' => 8,
    'pageSize' => 20
];