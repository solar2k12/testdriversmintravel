<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=test',
    'username' => 'test',
    'password' => 'test_password',
    'charset' => 'utf8'
];
